package com.taskaprosthetics.trainingtest

import android.graphics.*
import android.os.Bundle
import android.view.View
import android.view.animation.Animation
import android.view.animation.TranslateAnimation
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.res.ResourcesCompat
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*


class GameActivity : AppCompatActivity(),View.OnClickListener {

    private var gameViewUltimateX:Float = 0.0f
    var gameViewX :Float = 0.0f
    private var gameViewHeight:Int=0
    private var gameViewWidth:Int=0
    var yPos:Float=0.0f
    var move:Boolean=false
    private var shapeToggel:Boolean=false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // set onClick listener on buttons
        btn_zoom.setOnClickListener(this)
        btn_color.setOnClickListener(this)
        btn_move.setOnClickListener(this)
    }

    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.btn_move-> moveSquare()
            R.id.btn_zoom -> changeShape()
            R.id.btn_color -> changeRandomColor()
        }
    }

    private fun moveSquare() {
        //.....Get the device screen size in pixel.....
        val display = windowManager.defaultDisplay
        val size = Point()
        display.getSize(size)

        val windowHeight = size.y       //device screen height
        //..................................


        //Get Square Image X  position
         gameViewX = gameView.x


        //Get the Container Right Edge
        val containerRightEdge =
            cons_layout.width - cons_layout.paddingRight.toFloat()
        //Get the Square Image Width & Height
         gameViewWidth = gameView.width
         gameViewHeight = gameView.height

        //Calculate the ultimate X position of Square Image
         gameViewUltimateX = containerRightEdge - gameViewWidth

if(!move)
    yPos += gameViewHeight

        if(yPos+gameViewHeight>windowHeight)
        {

            yPos=2f
        }

        setAnim()

    }

    private fun setAnim() {
        val animation = TranslateAnimation((0).toFloat(), gameViewUltimateX, 0F, 0F)

        animation.fillAfter = true

        animation.duration = 3000

        gameView.startAnimation(animation)


        animation.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationStart(animation: Animation) {
move=true
            }
            override fun onAnimationRepeat(animation: Animation) {}
            override fun onAnimationEnd(animation: Animation) {
                move=false
                gameView.clearAnimation()
                gameView.y = yPos
                gameView.x = gameViewX
            }
        })
    }

    private fun changeShape() {                      // function to change the shape of square to round or vice-versa

        if (!shapeToggel) {
            gameView.background =
                ResourcesCompat.getDrawable(resources, R.mipmap.ic_launcher_round, null)
            shapeToggel = true
        } else {
            gameView.setBackgroundColor(Color.BLACK)
            shapeToggel = false
        }
    }

    private fun changeRandomColor() {       //function to select random color of square
        val rnd = Random()
        val color =
            Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256))
        gameView.setBackgroundColor(color)

    }


}
