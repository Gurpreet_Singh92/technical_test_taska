package com.taskaprosthetics.trainingtest

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Rect
import android.util.AttributeSet
import android.view.View
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

/**
 * What does this do? Dunno...
 */
class MyGameView@JvmOverloads
constructor(context:Context,attrs:AttributeSet?=null,defStyle:Int=0,defStyleRes:Int=0):View(context,attrs,defStyle,defStyleRes){
    private var moving = false

    private val squareSize = 50
    private var squareLeft = width
    private var squarePaint= Paint()

    init {
        //TODO: get them to do something with coroutines
        GlobalScope.launch {
            while(moving){
                if(moving){
                    squareLeft -= 100
                    if(squareLeft < 0)
                        squareLeft = width
                }

              //  Thread.sleep(100)
            }
        }
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)

                val squareTop = (height-squareSize)/2


        squarePaint.color=Color.BLACK

                    val square = Rect(
                        squareLeft-100, squareTop-100, squareLeft+squareSize-100, squareTop+squareSize-100
                    )

                    canvas?.drawRect(square, squarePaint)

invalidate()
requestLayout()
forceLayout()
}
}